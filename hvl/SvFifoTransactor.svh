`ifndef _AxSvFifoTransactor_svh_
`define _AxSvFifoTransactor_svh_

import svdpi::*;

class SvFifoTransactor;
	local chandle fifoTransactorPtr_;
	local event E_readDataDone_;
	local event E_readFullEmptyDone_;

	bit [ 31 : 0 ]	data;
	bit		full;
	bit		empty;

	static SvFifoTransactor fifoManager[chandle];

	local chandle hdl_scope_;
	
	/**
	 * Constructor of proxy
	 * @param hdlPath path to transactor on hw side
	 */
	function new( string hdlPath );
		hdl_scope_ = svGetScopeFromName( hdlPath );
		svc_setScope_SvFifoTransactorPkg();
		fifoManager[ hdl_scope_ ] = this;
	endfunction: new
	
	task writeData( input bit [ 31 : 0 ] _data, output bit _full, output bit _empty );
		svc_writeData( hdl_scope_, _data );
		@E_readFullEmptyDone_;
		_full = full;
		_empty = empty;
	endtask: writeData
	
	task readData( output bit [ 31 : 0 ] _data, output bit _full, output bit _empty );
		svc_readData( hdl_scope_ );
		//@E_readDataDone_;
		@E_readFullEmptyDone_;
		_data = data;
		_full = full;
		_empty = empty;
	endtask: readData
	
	function void setReadData( input bit [ 31 : 0 ] _data );
		data = _data;
		->E_readDataDone_;
	endfunction: setReadData

	function void setFullEmpty( input bit _full, input bit _empty );
		full = _full;
		empty = _empty;
		->E_readFullEmptyDone_;
	endfunction: setFullEmpty

endclass

/* DPI functions for connection with HW side */
import "DPI-C" context function void svc_setScope_SvFifoTransactorPkg();

import "DPI-C" context function void svc_writeData( input chandle _scope, input bit [ 31 : 0 ] _data );

import "DPI-C" context function void svc_readData( input chandle _scope );

export "DPI-C" function svc_sendReadData;
function void svc_sendReadData( input chandle _scope, input bit [ 31 : 0 ] _data );
	automatic SvFifoTransactor ptr = SvFifoTransactor::fifoManager[ _scope ];
	ptr.setReadData( _data );
endfunction

export "DPI-C" function svc_sendFullEmpty;
function void svc_sendFullEmpty( input chandle _scope, input bit _full, input bit _empty );
	automatic SvFifoTransactor ptr = SvFifoTransactor::fifoManager[ _scope ];
	ptr.setFullEmpty( _full, _empty );
endfunction

`endif // _AxSvMemoryTransactor_svh_
