module TB;

	import SvFifoTransactorPkg::*;

	SvFifoTransactor fifoXtor;

	integer iteration = 300;
	bit [ 31 : 0 ] wdata[255:0];
	bit [ 31 : 0 ] rdata;
	bit full;
	bit empty;

	initial begin

		fifoXtor = new( "UP.transactor" );

		#2ns;	// WAIT UNTIL AFTER XTOR IS OUT OF RST

		fifoXtor.readData(rdata, full, empty);
		$display("Read when empty, rdata: %0h, full: %0b, empty: %0b", rdata, full, empty);

		for ( integer i = 0; i < iteration; ++i ) begin: write_loop
			wdata[i] = $random;
			fifoXtor.writeData(wdata[i], full, empty);
			$display("WRITE_LOOP ITERATION #%0d, wdata: %0h, full: %0b, empty: %0b", i, wdata[i], full, empty);
		end: write_loop

		for ( integer j = 0; j < iteration; ++j ) begin: read_loop
			fifoXtor.readData(rdata, full, empty);
			$display("READ_LOOP ITERATION #%0d, rdata: %0h, full: %0b, empty: %0b:", j, rdata, full, empty);
		end: read_loop

		#10;
        	$finish;
	end

endmodule
