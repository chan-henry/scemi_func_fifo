module UP;
	parameter clockLow  = 1;
	parameter clockHigh = 1;
	parameter NUM_OF_EDGES_RESET = 5;

	bit clk;
	bit rst;

	bit re;
	bit we;
	bit [ 31 : 0 ] data_in;
	bit [ 31 : 0 ] data_out;

	bit empty;
	bit full;
	
	FifoTransactor transactor (
		.clk( clk ),
		.rst( rst ),
		.re( re ),
		.we( we ),
		.data_in( data_in ),
		.data_out( data_out ),
		.empty( empty ),
		.full( full )
	);

	fifo dut(
		.CLK( clk ),
		.RST( rst ),
		.WriteEn( we ),
		.ReadEn( re ),
		.DataIn( data_in ),
		.DataOut( data_out ),
		.Empty( empty ),
		.Full( full )
	);

	// Clock Generation
	initial
	begin
		clk <= 0;
		#clockHigh;
		forever 
		begin
			#clockLow  clk <= 1;
			#clockHigh clk <= 0;
		end
	end

	// Reset Generation
	initial begin
		rst <= 1;
		#1ns;
		rst <= 0;
	end
endmodule
