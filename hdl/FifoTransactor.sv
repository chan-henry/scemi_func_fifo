module FifoTransactor
( 
	input clk,
	input rst,

	output reg re,
	output reg we,
	output reg [ 31 : 0 ] data_in,
	input [ 31 : 0 ] data_out,

	input empty,
	input full
);
	export "DPI-C" function FifoTransactorInit;
	function void FifoTransactorInit();
		we = 0;
		re = 0;
		data_in = 0;
	endfunction

	bit [ 31 : 0 ] wData;
	bit readFlag = 0;
	bit writeFlag = 0;
	bit readProcessing = 0;
	bit writeProcessing = 0;

	typedef enum logic [2:0] {IDLE, RO, RW, WO, Receive} state;
	state current_state = IDLE;
	state next_state = IDLE;

	import "DPI-C" context function void sendReadData( bit [ 31 : 0 ] _data );
	import "DPI-C" context function void sendFullEmpty( bit _full, bit _empty );

	export "DPI-C" function readData;
	function void readData();
		readFlag = 1;
	endfunction

	export "DPI-C" function writeData;
	function void writeData( input bit [ 31 : 0 ] _data );
		wData = _data;
		writeFlag = 1;
	endfunction

	always@( posedge clk ) begin
		if( 1 == rst ) begin	// Synchronous reset
			current_state = IDLE;
			next_state = IDLE;
			we = 0;
			re = 0;
			data_in = 0;
		end
		else begin
			case(next_state)
				IDLE: begin
					current_state = IDLE;

					if( (1 == readFlag) && (0 == writeFlag) )begin
						next_state = RO;
						re = 1;
					end
					else if( (1 == readFlag) && (1 == writeFlag) )begin
						next_state = RW;
						re = 1;
						we = 1;
						data_in = wData;
					end
					else if( (0 == readFlag) && (1 == writeFlag) )begin
						next_state = WO;
						we = 1;
						data_in = wData;
					end
					else begin // 0 == readFlag && 0 == writeFlag
						next_state = IDLE;
						re = 0;
						we = 0;
					end
						
				end
				RO: begin
					current_state = RO;
					next_state = Receive;
					re = 0;
				end
				RW: begin
					current_state = RW;
					next_state = Receive;
					re = 0;
					we = 0;
				end
				WO: begin
					current_state = WO;
					next_state = Receive;
					we = 0;
				end
				Receive: begin
					current_state = Receive;
					next_state = IDLE;

					if(1 == readFlag)begin
						sendReadData( data_out );
					end

					sendFullEmpty(full, empty);

					readFlag = 0;
					writeFlag = 0;
				end
			endcase
		end
	end

endmodule: FifoTransactor
